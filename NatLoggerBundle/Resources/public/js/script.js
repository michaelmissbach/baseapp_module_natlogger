var padding = 20;
var natloggerData;

$(document).ready(function() {
    $('#sidebarToggle, #sidebarToggleTop').on('click',function () {
        redraw();
    });
    window.addEventListener('resize', function () {
        redraw();
    });
    window.addEventListener("orientationchange", function() {
        redraw();
    });
    natloggerDraw();
});

function redraw() {
    window.setTimeout(() => {
        natloggerDraw();
    }, 500);
}

function natloggerDraw() {
    for (var k in natloggerData){
        if (natloggerData.hasOwnProperty(k)) {
            draw(natloggerData[k]);
        }
    }
}     

function draw(data) {    
    let elem = document.querySelector('div.canvas' + data.selector);
    elem.innerHTML = '';

    let elapsed = document.querySelector('div.elapsed' + data.selector);
    let canvas = document.createElement('canvas');
    let width = elem.offsetWidth;

    canvas.width = width;
    canvas.height =50;
    elem.append(canvas);
    let ctx = canvas.getContext('2d');

    ctx.fillStyle = '#bac6c6';
    ctx.fillRect(0,0,canvas.width,canvas.height);

    ctx.fillStyle = '#001cc6';
    ctx.font = "10px Arial";
    for (var i = 0; i < 25; i++) {
        ctx.fillText(i, calculatePosition(width,i) - (i > 9 ? 5 : 2), 10);
    }

    ctx.strokeStyle = '#867e84';
    ctx.lineWidth = 1;
    ctx.beginPath();
    for (var i = 0; i < 24; i++) {
        for (var ii = 0; ii < 60; ii=ii+10) {
            let x = calculatePosition(width,i,ii);
            let y = 15;

            if ( ii % 60 > 0) {
                y = 25;
            }
            if ( ii % 30 > 0) {
                y = 30;
            }

            ctx.moveTo(x + 0.5 ,y );
            ctx.lineTo(x + 0.5,100);
        }
    }
    let x = calculatePosition(width,24,0);
    ctx.moveTo(x + 0.5 ,15 );
    ctx.lineTo(x + 0.5,100);

    ctx.stroke();

    ctx.strokeStyle = '#c60507';
    ctx.lineWidth = 5;
    ctx.beginPath();


    for (var k in data.list){
        //console.log(typeof data[k]);
        if (data.list.hasOwnProperty(k) && typeof data.list[k] === 'object') {

            let date = new Date(data.list[k].date);
            let x = calculatePosition(width,date.getHours(),date.getMinutes(),date.getSeconds());

            ctx.moveTo(x + 0.5 ,35);
            ctx.lineTo(x + 0.5,50);
        }
    }
    ctx.stroke();

    let elapsedObject = calculateToHoursAnMinutes(data.elapsed);
    elapsed.innerHTML = elapsedObject.hours + " Stunden " + elapsedObject.minutes + " Minuten";

}

function calculatePosition(width, hour = 0, minute = 0, second = 0)
{
    let width_inner = width - padding * 2;

    let smallest = width_inner / (24 * 60 * 60);
    let pos = 0;
    if (hour > 0) {
        pos = pos + (smallest * (hour * 60 * 60));
    }
    if (minute > 0) {
        pos = pos + (smallest * (minute * 60));
    }
    if (second > 0) {
        pos = pos + (smallest * second);
    }

    return pos + padding;
}

function calculateToHoursAnMinutes(minutes) {

    let result = {'hours':0,'minutes':0};

    let hours = parseInt(minutes / 60);

    if (hours > 0) {
        result.hours = hours;
        minutes = minutes - (hours * 60);
    }

    result.minutes = minutes;

    return result;
}