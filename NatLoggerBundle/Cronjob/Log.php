<?php

namespace NatLogger\NatLoggerBundle\Cronjob;

use BaseApp\BaseappBundle\Interfaces\IAppSettingGroups;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Service\SettingsService;
use Doctrine\Persistence\ManagerRegistry;
use NatLogger\NatLoggerBundle\Entity\Host;
use NatLogger\NatLoggerBundle\Entity\Job;
use BaseApp\BaseappBundle\Cronjob\ICronjob;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class Log
 * @package NatLogger\NatLoggerBundle\Cronjob
 */
class Log implements ICronjob, IAppSettingGroups, IAppSettings
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * Log constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     * @throws \Exception
     */
    public function run(ParameterBag $request, ParameterBag $parameterBag): void
    {
        $jobRepo = $this->doctrine->getRepository(Job::class);
        $hostRepo = $this->doctrine->getRepository(Host::class);

        $file = realpath(__DIR__."/scanner.jar");

        $jobs = $jobRepo->findBy(['active'=>true]);

        /** @var Job $job */
        foreach ($jobs as $job) {
            $args = sprintf('java -jar %s %s %s %s %s 2>&1',$file,$job->getType(),$job->getSubject(),$job->getParallelJobs(),$job->getTimeout());
            echo $args.PHP_EOL;
            $out = shell_exec($args);
            echo $out.PHP_EOL;
            $entries = json_decode($out);

            if (is_array($entries) && count($entries)) {

                $manager = $this->doctrine->getManager();
                $time = new \DateTime('now',new \DateTimeZone($job->getTimezone()));
                foreach ($entries as $entry) {

                    $host = $hostRepo->findOneBy(['host'=>$entry->host/*, 'ip'=>$entry->ip*/]);
                    if (!$host) {
                        $host = new Host();
                        $host->setIp($entry->ip);
                        $host->setHost($entry->host);
                        $manager->persist($host);
                    }

                    $log = new \NatLogger\NatLoggerBundle\Entity\Log();
                    $log->setTime($time)->setHost($host);
                    $manager->persist($log);
                }

                $manager->flush();
            }
        }
    }

    /**
     * @return int
     */
    public function getSecondInterval(): int
    {
        return (int)SettingsService::$instance->get(SettingsService::createSemanticKey('nat_logger','nat_logger_interval'),600);
    }

    public function getAppSettingGroups(): array
    {
        return [
            [
                SettingsService::SETTINGS_GROUP_KEY => 'nat_logger',
                SettingsService::SETTINGS_GROUP_NAME => 'Nat Logger'
            ]
        ];
    }

    public function getAppSettings(): array
    {
        return [
            [
                SettingsService::SETTINGS_KEY => 'nat_logger_interval',
                SettingsService::SETTINGS_GROUP_KEY => 'nat_logger',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Interval'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ],
            [
                SettingsService::SETTINGS_KEY => 'nat_logger_longest_interruption',
                SettingsService::SETTINGS_GROUP_KEY => 'nat_logger',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Longest interruption'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ]
        ];
    }
}