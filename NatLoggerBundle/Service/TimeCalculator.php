<?php

namespace NatLogger\NatLoggerBundle\Service;

use BaseApp\BaseappBundle\Service\SettingsService;


/**
 * Class TimeCalculator
 * @package NatLogger\NatLoggerBundle\Service
 */
class TimeCalculator
{
    /**
     * @var int
     */
    protected static $minTimeDiffInSeconds = 540;

    /**
     * @param array $list
     */
    public static function calculate(array &$list)
    {
        foreach ($list as $key=>&$entries) {
            $entries['elapsed'] = self::calculateEntry($entries['list']);
        }
    }

    /**
     * @param $entries
     * @return int
     */
    protected static function calculateEntry($entries)
    {
        $seconds = 0;
        /** @var \DateTime $last */
        $last = null;

        foreach($entries as $entry) {



            if ($last === null) {
                $last = $entry;
                continue;
            }

            $diff = $last->diff($entry);
            $diffInSeconds = ($diff->h * 60 * 60) + ($diff->i * 60) + $diff->s;
            if ($diffInSeconds < (int)SettingsService::$instance->get(SettingsService::createSemanticKey('nat_logger','nat_logger_longest_interruption'),1000)) {
                $seconds += $diffInSeconds;
            }

            $last = $entry;
        }

        $minutes = round($seconds / 60);
        if ($minutes > 1440) {
            $minutes = 1440;
        }

        return $minutes;
    }
}
