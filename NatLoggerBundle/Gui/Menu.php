<?php

namespace NatLogger\NatLoggerBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntryWithChilds;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuHeadline;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level2MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;

/**
 * Class Menu
 * @package NatLogger\NatLoggerBundle\Gui
 */
class Menu implements IGui
{
    /**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {
        $sidebar = $guiBuilder->getSidebar();
        $sidebar->add(Level1Divider::create());
        $sidebar->add(Level1MenuHeadline::create()->setTitle('Natlogger'));
        $sidebar->add(
            Level1MenuEntry::create()
                ->setTitle('Overview')->setIcon('fas fa-fw fa-tachometer-alt')->setRouteName('nat_index')
        );
        $container = $guiBuilder->createGuiContainer();
        $sidebar->add(
            Level1MenuEntryWithChilds::create()
                ->setChildContainer($container)
                ->setTitle('Config')->setIcon('fas fa-fw fa-table')
        );
        $container->add(Level2MenuEntry::create()->setRouteName('hosts')->setTitle('Hosts'));
        $container->add(Level2MenuEntry::create()->setRouteName('jobs')->setTitle('Jobs'));
    }

    public function modify(GuiBuilder $guiBuilder): void
    {
    }
}
