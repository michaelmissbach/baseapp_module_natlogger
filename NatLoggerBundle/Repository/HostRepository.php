<?php

namespace NatLogger\NatLoggerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NatLogger\NatLoggerBundle\Entity\Host;

/**
 * @method Host|null find($id, $lockMode = null, $lockVersion = null)
 * @method Host|null findOneBy(array $criteria, array $orderBy = null)
 * @method Host[]    findAll()
 * @method Host[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HostRepository extends EntityRepository
{
}
