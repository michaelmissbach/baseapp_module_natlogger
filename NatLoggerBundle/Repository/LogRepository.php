<?php

namespace NatLogger\NatLoggerBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use NatLogger\NatLoggerBundle\Entity\Log;
use BaseApp\BaseappBundle\Service\SettingsService;

/**
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends EntityRepository
{
    /**
     * @param $date
     * @return array
     * @throws \Exception
     */
    public function getStructuredByDate($date)
    {
        $startInterval = new \DateInterval(sprintf('PT%sS',SettingsService::$instance->get(SettingsService::createSemanticKey('nat_logger','nat_logger_interval'),600)));
        $startInterval->invert = 1;
        $start = (new \DateTime(sprintf('%s 0:00',$date->format('Y-m-d'))))->add($startInterval);

        $endInterval = new \DateInterval(sprintf('PT%sS',SettingsService::$instance->get(SettingsService::createSemanticKey('nat_logger','nat_logger_interval'),600)));
        $end = (new \DateTime(sprintf('%s 23:59:59',$date->format('Y-m-d'))))->add($endInterval);

        /** @var QueryBuilder $querybuilder */
        $querybuilder = $this->createQueryBuilder('l');
        $querybuilder
            ->select('l')
            ->where('l.time > :start')
            ->andWhere('l.time < :end')
            ->setParameter('start',$start)
            ->setParameter('end',$end)
        ;
        $natlogs = $querybuilder->getQuery()->getResult();

        $list = [];

        /** @var Log $natlog */
        foreach ($natlogs as $natlog) {

            $selector = sprintf('%s - %s',$natlog->getHost()->getIp(),$natlog->getHost()->getHost());
            $selector = str_replace('=','',base64_encode($selector));
            if (!array_key_exists($selector,$list)) {
                $list[$selector] = [];
                $list[$selector]['selector'] = $selector;
                $list[$selector]['name'] = $natlog->getHost()->getName();
                $list[$selector]['known'] = $natlog->getHost()->getKnown();
                $list[$selector]['ip'] = $natlog->getHost()->getIp();
                $list[$selector]['hostid'] = $natlog->getHost()->getId();
                $list[$selector]['host'] = $natlog->getHost()->getHost();
                $list[$selector]['title'] = $natlog->getHost()->getName()?:$natlog->getHost()->getHost();
                $list[$selector]['list'] = [];
            }
            $list[$selector]['list'][] = $natlog->getTime();
        }

        return $list??[];
    }

    /**
     * @param $hostId
     * @return array
     */
    public function getStructuredByHostId($hostid)
    {
        /** @var QueryBuilder $querybuilder */
        $querybuilder = $this->createQueryBuilder('l');
        $querybuilder
            ->select('l')
            ->where('l.host = :hostid')
            ->setParameter('hostid',$hostid)
        ;
        $natlogs = $querybuilder->getQuery()->getResult();

        $list = [];

        /** @var Log $natlog */
        foreach ($natlogs as $natlog) {

            $selector = sprintf('%s',$natlog->getTime()->format('Y-m-d'));
            if (!array_key_exists($selector,$list)) {
                $list[$selector] = [];
                $list[$selector]['selector'] = $selector;
                $list[$selector]['name'] = $natlog->getHost()->getName();
                $list[$selector]['known'] = $natlog->getHost()->getKnown();
                $list[$selector]['ip'] = $natlog->getHost()->getIp();
                $list[$selector]['host'] = $natlog->getHost()->getHost();
                $list[$selector]['title'] = $natlog->getHost()->getName()?:$natlog->getHost()->getHost();
                $list[$selector]['list'] = [];
            }
            $list[$selector]['list'][] = $natlog->getTime();
        }

        return $list??[];
    }
}
