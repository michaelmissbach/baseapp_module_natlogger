<?php

namespace NatLogger\NatLoggerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NatLogger\NatLoggerBundle\Entity\Job;

/**
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends EntityRepository
{
}
