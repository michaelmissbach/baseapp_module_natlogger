<?php

namespace NatLogger\NatLoggerBundle\Form;


use NatLogger\NatLoggerBundle\Entity\Host;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HostFormType
 * @package NatLogger\NatLoggerBundle\Form
 */
class HostFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('ip', TextType::class, [
                'label' => 'Ip',
                'empty_data' => '',
                'required' => false,
                'attr' => array(
                    'readonly' => true,
                ),
            ])
            ->add('host', TextType::class, [
                'label' => 'Host',
                'empty_data' => '',
                'required' => false,
                'attr' => array(
                    'readonly' => true,
                ),
            ])
            ->add('name', TextType::class, [
                'label' => 'Name',
                'empty_data' => '',
                'required' => false
            ])
            ->add('known', CheckboxType::class, [
                'label' => 'Known host',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Host::class
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'host_form';
    }
}