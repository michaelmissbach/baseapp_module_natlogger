<?php

namespace NatLogger\NatLoggerBundle\Form;

use NatLogger\NatLoggerBundle\Entity\Job;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JobFormType
 * @package NatLogger\NatLoggerBundle\Form
 */
class JobFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'type',
                'required' => false,
                'choices' => Job::choices()
            ])
            ->add('timezone', ChoiceType::class, [
                'label' => 'Timezone',
                'required' => false,
                'choices' => Job::timezones()
            ])
            ->add('subject', TextType::class, [
                'label' => 'Subject',
                'required' => false,
            ])
            ->add('parallelJobs', TextType::class, [
                'label' => 'Parallel worker count',
                'required' => false,
            ])
            ->add('timeout', TextType::class, [
                'label' => 'Timeout ms',
                'required' => false,
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Active',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Job::class
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'job_form';
    }
}