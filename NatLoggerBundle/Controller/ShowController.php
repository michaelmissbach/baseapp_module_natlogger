<?php

namespace NatLogger\NatLoggerBundle\Controller;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use NatLogger\NatLoggerBundle\Entity\Log;
use NatLogger\NatLoggerBundle\Service\TimeCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShowController
 * @package NatLogger\NatLoggerBundle\Controller
 */
class ShowController extends AbstractController
{
    public function index(Request $request)
    {
        GuiBuilder::$instance->getHeadJavascripts()
            ->add(
                InternJavascript::create()->setTitle('/bundles/natlogger/js/script.js'),
                10
            );

        $date = new \DateTime($request->request->get('date','now'));

        $list = $this->getDoctrine()->getRepository(Log::class)->getStructuredByDate($date);

        TimeCalculator::calculate($list);

        return $this->render('@NatLogger/show/main.html.twig',
            [
                'date'=>$date->format('d.m.Y'),
                'date_datepicker'=>$date->format('m/d/Y'),
                'list'=>$list,
                'json_list'=>json_encode($list),
                'interval' => 5
            ]
        );
    }

    public function detail(Request $request,$hostid)
    {
        GuiBuilder::$instance->getHeadJavascripts()
            ->add(
                InternJavascript::create()->setTitle('/bundles/natlogger/js/script.js'),
                10
            );

        $list = $this->getDoctrine()->getRepository(Log::class)->getStructuredByHostId($hostid);

        TimeCalculator::calculate($list);

        return $this->render('@NatLogger/show/detail.html.twig',
            [
                'list'=>$list,
                'json_list'=>json_encode($list),
                'interval' => 5
            ]
        );
    }
}
