<?php

namespace NatLogger\NatLoggerBundle\Controller;

use NatLogger\NatLoggerBundle\Entity\Host;
use NatLogger\NatLoggerBundle\Form\HostFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HostController
 * @package NatLogger\NatLoggerBundle\Controller
 */
class HostController  extends AbstractController
{

    public function index()
    {
        $hosts = $this->getDoctrine()->getRepository(Host::class)->findAll();
        return $this->render('@NatLogger/host/index.html.twig',['hosts'=>$hosts]);
    }

    public function delete($id)
    {
        $host = $this->getDoctrine()->getRepository(Host::class)->find($id);
        $this->getDoctrine()->getManager()->remove($host);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('hosts'));
    }

    public function create()
    {
        $host = new Host();
        $host->setIp('test');
        $this->getDoctrine()->getManager()->persist($host);
        $this->getDoctrine()->getManager()->flush($host);


        return $this->redirect($this->generateUrl('hosts'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request,$id)
    {
        $host = $this->getDoctrine()->getRepository(Host::class)->find($id);

        $form = $this->createForm(HostFormType::class,$host);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $host = $form->getData();
            $this->getDoctrine()->getManager()->persist($host);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('hosts'));

        }

        return $this->render('@NatLogger/host/update.html.twig',['form'=>$form->createView()]);
    }
}
