<?php

namespace NatLogger\NatLoggerBundle\Controller;

use NatLogger\NatLoggerBundle\Entity\Job;
use NatLogger\NatLoggerBundle\Form\JobFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JobController
 * @package NatLogger\NatLoggerBundle\Controller
 */
class JobController  extends AbstractController
{
    public function index()
    {
        $jobs = $this->getDoctrine()->getRepository(Job::class)->findAll();
        return $this->render('@NatLogger/job/index.html.twig',['jobs'=>$jobs]);
    }

    public function delete($id)
    {
        $job = $this->getDoctrine()->getRepository(Job::class)->find($id);
        $this->getDoctrine()->getManager()->remove($job);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('jobs'));
    }

    public function create()
    {
        $choices = Job::choices();
        $choice = reset($choices);

        $job = new Job();
        $job->setType($choice);
        $this->getDoctrine()->getManager()->persist($job);
        $this->getDoctrine()->getManager()->flush($job);


        return $this->redirect($this->generateUrl('jobs'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request,$id)
    {
        $job = $this->getDoctrine()->getRepository(Job::class)->find($id);

        $form = $this->createForm(JobFormType::class,$job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $job = $form->getData();
            $this->getDoctrine()->getManager()->persist($job);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('jobs'));

        }

        return $this->render('@NatLogger/job/update.html.twig',['form'=>$form->createView()]);
    }
}
