<?php

namespace NatLogger\NatLoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="NatLogger\NatLoggerBundle\Repository\LogRepository")
 */
class Log
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity="NatLogger\NatLoggerBundle\Entity\Host", inversedBy="logs")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     */
    private $host;

    public function getId()
    {
        return $this->id;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime( $time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Log
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }
}
