<?php

namespace NatLogger\NatLoggerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Host
 * @package NatLogger\NatLoggerBundle\Entity
 * @ORM\Entity(repositoryClass="NatLogger\NatLoggerBundle\Repository\HostRepository")
 */
class Host
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $host = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private $known = false;

    /**
     * @ORM\OneToMany(targetEntity="NatLogger\NatLoggerBundle\Entity\Log", mappedBy="host")
     */
    private $logs;

    /**
     * Host constructor.
     */
    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Host
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     * @return Host
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Host
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Host
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKnown()
    {
        return $this->known;
    }

    /**
     * @param mixed $known
     * @return Host
     */
    public function setKnown($known)
    {
        $this->known = $known;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param mixed $logs
     * @return Host
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;
        return $this;
    }

    /**
     * @param mixed $log
     * @return Host
     */
    public function addLog($log)
    {
        $this->logs[] = $log;
        return $this;
    }
}