<?php

namespace NatLogger\NatLoggerBundle\Entity;

use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Job
 * @package NatLogger\NatLoggerBundle\Entity
 * @ORM\Entity(repositoryClass="NatLogger\NatLoggerBundle\Repository\JobRepository")
 */
class Job
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timezone = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $parallelJobs = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timeout = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Job
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Job
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Job
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Job
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param mixed $timezone
     * @return Job
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParallelJobs()
    {
        return $this->parallelJobs;
    }

    /**
     * @param mixed $parallelJobs
     * @return Job
     */
    public function setParallelJobs($parallelJobs)
    {
        $this->parallelJobs = $parallelJobs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param mixed $timeout
     * @return Job
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    public static function choices()
    {
        return [
            'ip'  => 'ip',
            'url' => 'url'
        ];
    }

    /**
     * @return array
     */
    public static function timezones()
    {
        //$list = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        foreach (DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $key=>$entry) {
            yield $entry=>$entry;
        }
    }
}
