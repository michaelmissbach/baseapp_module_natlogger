<?php

namespace NatLogger\NatLoggerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class NatLoggerBundle
 * @package NatLogger\NatLoggerBundle
 */
class NatLoggerBundle extends Bundle
{
    const MAJOR_VERSION = 1;
    const MINOR_VERSION = 0;
    const REVISION_VERSION = 6;

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}
