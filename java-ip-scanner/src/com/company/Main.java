package com.company;

import com.company.Builder.Builder;
import com.company.Collector.Collector;
import com.company.JsonFormatter.Format;

public class Main {
    public static void main(String[] args) {
        Collector c;
        String type = "";
        String content = "";
        String workers = "1";
        String timeout = "100";
        if (args.length == 0) {
            System.out.println("Error: Please specify url/ip.");
            System.exit(1);
        }
        if (args.length > 0 && args[0] != null)
            type = args[0];
        if (args.length > 1 && args[1] != null)
            content = args[1];
        if (args.length > 2 && args[2] != null)
            workers = args[2];
        if (args.length > 3 && args[3] != null)
            timeout = args[3];
        Builder b = new Builder();
        switch (type) {
            case "url":
                b.runUrl(content);
                break;
            case "ip":
                c = new Collector();
                c.range(content);
                b.runIps(c.getIps(), Integer.parseInt(workers), Integer.parseInt(timeout));
                break;
            default:
                System.out.println("Error: Unknown type " + type);
                System.exit(1);
                break;
        }
        Format f = new Format();
        System.out.println(f.run(b.getResolvedHosts()));
    }
}
