package com.company.Builder;

import com.company.Dto.Ip;
import com.company.Dto.ResolvedHost;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Builder {
    private List<Ip> list;

    private List<ResolvedHost> resolvedHosts = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<Callable<Object>> calls = new ArrayList<>();

    public void runIps(List<Ip> list, int atonce, int timeout) {
        this.list = list;
        int counter = 1;
        for (Ip ip : this.list) {
            Task t = new Task();
            t.setIp(ip);
            t.setTimeout(timeout);
            this.tasks.add(t);
            this.calls.add(Executors.callable(t));
            counter++;
            if (counter > atonce) {
                workIps();
                counter = 1;
            }
        }
        if (this.calls.size() > 0)
            workIps();
    }

    private void workIps() {
        ExecutorService pool = Executors.newFixedThreadPool(this.calls.size());
        try {
            pool.invokeAll(this.calls);
            for (Task t : this.tasks) {
                ResolvedHost r = t.getResolvedHost();
                if (r != null)
                    this.resolvedHosts.add(r);
            }
        } catch (Exception exception) {}
        pool.shutdown();
        this.tasks.clear();
        this.calls.clear();
    }

    public void runUrl(String address) {
        try {
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.connect();
            int code = connection.getResponseCode();
            if (code < 400) {
                String host = url.getHost();
                InetAddress add = InetAddress.getByName(host);
                String ip = add.getHostAddress();
                this.resolvedHosts.add(new ResolvedHost(ip, host));
            }
        } catch (Exception exception) {}
    }

    public List<ResolvedHost> getResolvedHosts() {
        return this.resolvedHosts;
    }
}
