package com.company.Builder;

import com.company.Dto.Ip;
import com.company.Dto.ResolvedHost;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Task implements Runnable {
    private Ip ip;

    private int timeout = 100;

    private ResolvedHost resolvedHost;

    private int[] ports;

    public void run() {
        byte[] b = { (byte)this.ip.getA(), (byte)this.ip.getB(), (byte)this.ip.getC(), (byte)this.ip.getD() };
        checkByInetAddres(b);
    }

    private void checkByInetAddres(byte[] b) {
        try {
            InetAddress addr = InetAddress.getByAddress(b);
            if (addr.isReachable(this.timeout))
                this.resolvedHost = new ResolvedHost(addr.getHostAddress(), addr.getHostName());
        } catch (UnknownHostException unknownHostException) {

        } catch (Exception exception) {}
    }

    public void setIp(Ip ip) {
        this.ip = ip;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public ResolvedHost getResolvedHost() {
        return this.resolvedHost;
    }
}
