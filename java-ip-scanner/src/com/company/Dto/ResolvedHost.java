package com.company.Dto;

public class ResolvedHost {
    private String ip;

    private String host;

    public ResolvedHost(String ip, String host) {
        this.ip = ip;
        this.host = host;
    }

    public String getIp() {
        return this.ip;
    }

    public String getHost() {
        return this.host;
    }
}
