package com.company.JsonFormatter;

import com.company.Dto.ResolvedHost;
import java.util.List;

public class Format {
    public String run(List<ResolvedHost> resolvedHostList) {
        StringBuilder b = new StringBuilder();
        for (ResolvedHost r : resolvedHostList) {
            if (b.length() > 0)
                b.append(",");
            b.append("{\"host\":\"");
            b.append(r.getHost());
            b.append("\",\"ip\":\"");
            b.append(r.getIp() + "\"}");
        }
        b.insert(0, "[");
        b.append("]");
        return b.toString();
    }
}
