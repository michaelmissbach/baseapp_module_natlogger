package com.company.Collector;

import com.company.Dto.Ip;
import com.company.Dto.Range;
import java.util.ArrayList;
import java.util.List;

public class Collector {
    Range a;

    Range b;

    Range c;

    Range d;

    List<Ip> ips = new ArrayList<>();

    public void range(String input) {
        String[] segments = input.split("\\.");
        this.a = create(segments[0]);
        this.b = create(segments[1]);
        this.c = create(segments[2]);
        this.d = create(segments[3]);
        for (int aa = this.a.getFrom(); aa <= this.a.getTo(); aa++) {
            for (int bb = this.b.getFrom(); bb <= this.b.getTo(); bb++) {
                for (int cc = this.c.getFrom(); cc <= this.c.getTo(); cc++) {
                    for (int dd = this.d.getFrom(); dd <= this.d.getTo(); dd++)
                        this.ips.add(new Ip(aa, bb, cc, dd));
                }
            }
        }
    }

    protected Range create(String range) {
        if (range.equals("x"))
            return new Range(0, 255);
        if (range.contains("-")) {
            String[] tmp = range.split("-");
            return new Range(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1]));
        }
        return new Range(Integer.parseInt(range), Integer.parseInt(range));
    }

    public List<Ip> getIps() {
        return this.ips;
    }
}
